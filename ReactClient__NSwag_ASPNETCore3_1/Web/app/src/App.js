import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    React With ASP.Net Core 3.2 using NSwag to generate ReactClient
        </p>
                <a
                    className="App-link"
                    href="https://gitlab.com/RajneeshRocks/aspnetcore-and-react-using-nswag-ms-build"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Source code
        </a>
            </header>
        </div>
    );
}

export default App;
