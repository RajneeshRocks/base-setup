﻿using API.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Infrastructure
{
    public class RockContext : DbContext
    {
        public RockContext(DbContextOptions<RockContext> options) : base(options)
        {

        }
        public DbSet<Person> Persons { get; set; }
    }
}
