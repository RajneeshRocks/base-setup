﻿using API.Entity;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Infrastructure
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<API.Entity.Person, API.ViewModel.Person>();
            CreateMap<API.ViewModel.Person, API.Entity.Person>();
        }
    }
}
