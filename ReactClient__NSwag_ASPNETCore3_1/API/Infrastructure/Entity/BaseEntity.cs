﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Entity
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            this.CreatedDate = DateTime.Now;
        }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
