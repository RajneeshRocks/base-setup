﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.Entity
{
    public class Person : BaseEntity
    {
        [Key, Column("Id")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProfileImage { get; set; }
    }
}
