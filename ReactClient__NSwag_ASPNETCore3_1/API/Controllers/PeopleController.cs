﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Entity;
using API.Infrastructure;
using AutoMapper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly RockContext _context;
        private readonly IMapper _imapper;

        public PeopleController(RockContext context, IMapper imapper)
        {
            _context = context;
            _imapper = imapper;
        }

        // GET: api/People
        [HttpGet]
        public async Task<ActionResult<IEnumerable<API.ViewModel.Person>>> GetPersons()
        {
            var entity = await _context.Persons.ToListAsync();
            return _imapper.Map<List<API.ViewModel.Person>>(entity);
        }

        // GET: api/People/5
        [HttpGet("{id}")]
        public async Task<ActionResult<API.ViewModel.Person>> GetPerson(int id)
        {
            var person = await _context.Persons.FindAsync(id);

            if (person == null)
            {
                return NotFound();
            }

            return _imapper.Map<API.ViewModel.Person>(person);
        }

        // PUT: api/People/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, API.ViewModel.Person person)
        {
            if (id != person.Id)
            {
                return BadRequest();
            }

            var entity = _imapper.Map<API.Entity.Person>(person);
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/People
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<API.ViewModel.Person>> PostPerson(API.ViewModel.Person person)
        {
            var entity = _imapper.Map<API.Entity.Person>(person);
            _context.Persons.Add(entity);
            await _context.SaveChangesAsync();

            var prsn = _imapper.Map<API.ViewModel.Person>(entity);
            return CreatedAtAction("GetPerson", new { id = prsn.Id }, prsn);
        }

        // DELETE: api/People/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<API.ViewModel.Person>> DeletePerson(int id)
        {

            var person = await _context.Persons.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }

            _context.Persons.Remove(person);
            await _context.SaveChangesAsync();

            var model = _imapper.Map<API.ViewModel.Person>(person);
            return model;
        }

        private bool PersonExists(int id)
        {
            return _context.Persons.Any(e => e.Id == id);
        }
    }
}
