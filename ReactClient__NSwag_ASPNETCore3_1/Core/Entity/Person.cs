﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity
{
    public class Person : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProfileImage { get; set; }
    }
}
