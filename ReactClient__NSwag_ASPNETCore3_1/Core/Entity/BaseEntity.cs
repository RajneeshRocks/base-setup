﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity
{
    public abstract class BaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
